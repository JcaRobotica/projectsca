#include <controller_interface/controller.h>
#include <hardware_interface/joint_command_interface.h>
#include <pluginlib/class_list_macros.h>
#include <boost/algorithm/clamp.hpp>
#include <boost/algorithm/minmax.hpp>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include "ros/ros.h"
#include <kdl_parser/kdl_parser.hpp>
#include <urdf/model.h>
#include <kdl/chaindynparam.hpp>
using namespace std;


namespace controller_ns{

class PositionController : public controller_interface::Controller<hardware_interface::EffortJointInterface>
{
public:
  bool init(hardware_interface::EffortJointInterface* hw, ros::NodeHandle &n)
  {
    
    ficheroSalida.open ("documento.txt");
    d_error_ = 0;
    i_error_ = 0;
    error_last_ = 0;
    std::string my_joint;
    if (!n.getParam("joint", my_joint)){
      ROS_ERROR("Could not find joint name");
      return false;
    }

    joint_ = hw->getHandle(my_joint);  // throws on failure
    return true;
  }

  void update(const ros::Time& time, const ros::Duration& period)
  {
    double jpos=joint_.getPosition();
    double error = setpoint_ - jpos;
    

    double p_term, d_term, i_term;

    d_error_ = (error - error_last_) / period.toSec();
    error_last_ = error;
    
    p_term=error*gain_;
    i_error_ += period.toSec() * error;
    boost::tuple<double, double> bounds = boost::minmax<double>(i_min_ / i_gain_, i_max_ / i_gain_);
    i_error_ = boost::algorithm::clamp(i_error_, bounds.get<0>(), bounds.get<1>());

    i_term = i_gain_ * i_error_;

    if(i_term>i_max_) i_term=i_max_;
    if(i_term<i_min_) i_term=i_min_;
    
    d_term=d_error_*d_gain_;

    double PID= p_term + d_term + i_term;
    

    ROS_INFO("%f",error);
    joint_.setCommand(PID);
    
  }

  void starting(const ros::Time& time) { }
  void stopping(const ros::Time& time) { }

private:
  static const double d_gain_ = 30.0;
  static const double i_gain_ = 18.0;
  static const double i_max_ = 0.3;
  static const double i_min_ = -0.3;
  ofstream ficheroSalida;

  double d_error_;
  double i_error_;
  double error_last_;
  hardware_interface::JointHandle joint_;
  static const double gain_ = 180.0;
  static const double setpoint_ = 1.00;
};
PLUGINLIB_EXPORT_CLASS(controller_ns::PositionController, controller_interface::ControllerBase);
}

