#include <controller_interface/controller.h>
#include <hardware_interface/joint_command_interface.h>
#include <pluginlib/class_list_macros.h>
#include <boost/algorithm/clamp.hpp>
#include <boost/algorithm/minmax.hpp>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <fstream>
#include "ros/ros.h"
#include <kdl_parser/kdl_parser.hpp>
#include <urdf/model.h>
#include <kdl/chaindynparam.hpp>


using namespace std;


namespace controller_ns{

class PositionController : public controller_interface::Controller<hardware_interface::EffortJointInterface>
{
public:
  bool init(hardware_interface::EffortJointInterface* hw, ros::NodeHandle &n)
{
	kp_=KDL::JntArray(2);
	kd_=KDL::JntArray(2);
	// Se define el punto de consigna para las dos articulaciones
	setpoint_=KDL::JntArray(2);
	setpoint_(0) = 1.57; setpoint_(1) = 0.785;
	std::vector<double> kps_;
	std::vector<double> kvs_;
	
	kp_(0) = 100; kp_(1) = 100;
	kd_(0) = 10; kd_(1) = 10;
	// Se obtienen las ganancias proporcional y derivativa del fichero YAML
	std::string param_name;
	
	// Lista de articulaciones controladas (2 en este robot)
	param_name = "joints";
	if(!n.getParam(param_name, joint_names_))
	{
		ROS_ERROR_STREAM("Failed to getParam '" << param_name << "' (namespace: " <<
		n.getNamespace() << ").");
		return false;
	}
	// Articulaciones controladas
	n_joints_ = joint_names_.size();
	if(n_joints_ == 0){
		ROS_ERROR_STREAM("List of joint names is empty.");
		return false;
	}
	// Se obtiene el arbol KDL a partir del urdf
	KDL::Tree my_tree;
	urdf::Model my_model;
	std::string file = "/home/julio/tiago_public_ws/src/tiago_robot/tiago_description/robots/tiago.urdf";
	// Leyendo el fichero urdf generado a partir del xacro
	if(!my_model.initFile(file)){
		ROS_ERROR("Failed to parse urdf robot model");
		return false;
	}
	if (!kdl_parser::treeFromUrdfModel(my_model, my_tree)){
		ROS_ERROR("Failed to construct kdl tree");
		return false;
	}
	// Generado el KDL Tree completo
	// Nos quedamos en chain la cadena cinemática completa del brazo
	my_tree.getChain("arm_1_link", "arm_2_link",chain);
	std::cout<<chain.getNrOfJoints()<<std::endl;
	std::cout<<"tamaño: "<<joint_names_.size()<<std::endl;
	// Recorremos las articulaciones que van a ser controladas (2 para el robot)
	for(unsigned int i=0; i<n_joints_; i++)
	{
		try
		{
			std::cout << "push_back"<< joint_names_[i] << std::endl;
			joints_.push_back(hw->getHandle(joint_names_[i]));
		}
		catch (const hardware_interface::HardwareInterfaceException& e)
		{
			ROS_ERROR_STREAM("Exception thrown: " << e.what());
			return false;
		}
	}
	

	return true;
}

  void update(const ros::Time& time, const ros::Duration& period)
{
	
	// Definición del controlador
	double error;
	double commanded_effort;
	double current_position, current_velocity;
	double P=0;
	double D=0;
	for(unsigned int i=0; i<n_joints_; i++)
	{
		current_position = joints_[i].getPosition();
		
		// Compute position error
		error = setpoint_(i) - current_position;
		std::cout << "error( "<< i <<") = "<<error<<std::endl;
		// Control command
		P = error * kp_(i);
		d_error_ = (error - error_last_) / period.toSec();
		D = d_error_ * kd_(i);
		error_last_ = error;

		commanded_effort = P + D;
		joints_[i].setCommand(commanded_effort);
	}
}

  void starting(const ros::Time& time) { }
  void stopping(const ros::Time& time) { }

private:
	// hardware_interface::JointHandle joint_;
	std::vector< std::string > joint_names_;
	std::vector< hardware_interface::JointHandle > joints_;
	unsigned int n_joints_;
	KDL::Chain chain;
	// Ganancias y referencias
	KDL::JntArray kp_;
	KDL::JntArray kd_;
	KDL::JntArray setpoint_;
	double d_error_;
	double error_last_;
};
PLUGINLIB_EXPORT_CLASS(controller_ns::PositionController, controller_interface::ControllerBase);
}

